defmodule Elixonym.Dictionary do
  @csv_path "../../data/words.csv"

  def read_words() do
    Application.get_env(:elixonym, :csv_path, @csv_path)
    |> Path.expand(__DIR__)
    |> File.read!()
    |> String.split(",")
    |> Enum.map(fn word ->
      word
    end)
  end
end
