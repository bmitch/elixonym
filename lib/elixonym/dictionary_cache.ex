defmodule Elixonym.DictionaryCache do
  use GenServer
  alias Elixonym.Dictionary

  # two hours
  @refresh_interval :timer.seconds(3600 * 2)

  def start_link(_args) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def get_word() do
    GenServer.call(__MODULE__, :get_word)
  end

  def init(:ok) do
    state = load_words()
    schedule_refresh()
    {:ok, state}
  end

  def handle_call(:get_word, _from, state) do
    {:reply, Enum.random(state), state}
  end

  def handle_info(:refresh, _state) do
    state = load_words()
    schedule_refresh()
    {:noreply, state}
  end

  def handle_info(_any, state) do
    {:noreply, state}
  end

  defp load_words, do: Dictionary.read_words()
  defp schedule_refresh, do: Process.send_after(self(), :refresh, @refresh_interval)
end
