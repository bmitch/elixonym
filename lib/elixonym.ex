defmodule Elixonym do
  alias Elixonym.DictionaryCache

  def get_words(number_of_words) do
    {_, word} =
      Enum.map_reduce(1..number_of_words, "", fn word, acc ->
        {word, get_word() <> " " <> acc}
      end)

    word
    |> String.trim()
  end

  defp get_word() do
    DictionaryCache.get_word()
    |> String.capitalize()
  end
end
