defmodule DictionaryCacheTest do
  use ExUnit.Case

  test "it can return a random word" do
    assert Elixonym.DictionaryCache.get_word() == "foo"
  end
end
