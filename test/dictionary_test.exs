defmodule DictionaryTest do
  use ExUnit.Case

  test "it can read the configured words data" do
    assert Elixonym.Dictionary.read_words() == ["foo"]
  end
end
