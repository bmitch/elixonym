defmodule ElixonymTest do
  use ExUnit.Case

  test "it can get random words from the dictionary cache" do
    assert Elixonym.get_words(1) == "Foo"
    assert Elixonym.get_words(2) == "Foo Foo"
    assert Elixonym.get_words(5) == "Foo Foo Foo Foo Foo"
  end
end
